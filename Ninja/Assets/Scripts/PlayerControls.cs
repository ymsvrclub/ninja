﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
	Rigidbody rigidbody;
	CapsuleCollider collider;
	public float movementVelocity;
	public float sensitivity;

	[SerializeField]
	Vector3 direction;

	Camera camera;

	// public bool grounded;
	// public float jumpPower;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
		collider = GetComponent<CapsuleCollider>();
		camera = Camera.main;
		direction = new Vector3(0, 0, 1);
		Cursor.lockState = CursorLockMode.Locked;
	}

	void Update()
	{
		var movementX = Input.GetAxis("Horizontal");
		var movementZ = Input.GetAxis("Vertical");

		var rotationX = -Input.GetAxis("Mouse Y");
		var rotationY = Input.GetAxis("Mouse X");
		rotationY = Mathf.Clamp(rotationY, -90f, 90f);

		transform.GetChild(0).eulerAngles += new Vector3(rotationX, rotationY, 0) * sensitivity;

		if (Input.GetKey(KeyCode.Escape))
		{
			if (Cursor.lockState != CursorLockMode.None)
			{
				Cursor.lockState = CursorLockMode.None;
			}
			else
			{
				Cursor.lockState = CursorLockMode.Locked;
			}
		}

		var movement = new Vector3(movementX, 0, movementZ);
		
		var cameraForward = camera.transform.forward;
		cameraForward.y = 0;
		movement = Quaternion.LookRotation(cameraForward) * movement;
		movement.y = 0;

		rigidbody.velocity = movement * movementVelocity;
	}
}
