﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] prefabs;
    public float respawnTime;

    public Transform[] positions;

    public Queue<GameObject> pool;
    public int poolSize;

    // Start is called before the first frame update
    void Start()
    {
        pool = new Queue<GameObject>();
        for (int i = 0; i < poolSize; i++)
        {
            var prefabIndex = Random.Range(0, prefabs.Length);
            GameObject obj = Instantiate(prefabs[prefabIndex]);
            obj.SetActive(false);
            pool.Enqueue(obj);
        }
        StartCoroutine(wave());
    }

    private void Spawn()
    {
        GameObject obj = pool.Dequeue();

        if (obj.activeSelf)
        {
            Debug.LogError("AAAAAAAAAAAAA");
        }

        var posIndex = Random.Range(0, positions.Length);
        obj.transform.position = positions[posIndex].position;
        obj.SetActive(true);

        pool.Enqueue(obj);
    }

    IEnumerator wave()
    {
        while (true)
        {
            yield return new WaitForSeconds(respawnTime);
            Spawn();
        }
    }
}
