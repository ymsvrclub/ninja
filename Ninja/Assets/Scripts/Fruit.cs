﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class Fruit : MonoBehaviour
{
    Rigidbody rigidbody;

    public float totalTime;

    public GameObject rect;

    public Vector3 startVelocity;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        ComputeStartVelocity();
        Launch();
    }

    void ComputeStartVelocity()
    {
        var t = rect.transform;

        var left = t.position.x - t.localScale.x / 2;
        var right = t.position.x + t.localScale.x / 2;
        var top = t.position.z - t.localScale.z / 2;
        var bottom = t.position.z + t.localScale.z / 2;

        var target = new Vector3(Random.Range(left, right), t.position.y, Random.Range(top, bottom));

        var distance = target - transform.position;

        startVelocity = distance / totalTime;
        startVelocity.y -= Physics.gravity.y * totalTime / 2;
    }

    void Launch()
    {
        if (rigidbody)
        {
            rigidbody.velocity = new Vector3();
            rigidbody.AddForce(startVelocity * rigidbody.mass, ForceMode.Impulse);
        }
    }

    void OnEnable()
    {
        ComputeStartVelocity();
        Launch();
    }

    void FixedUpdate()
    {
        if (transform.position.magnitude > 300f)
        {
            this.gameObject.SetActive(false);
        }
        rigidbody.AddTorque(5, 5, 0, ForceMode.Impulse);
    }
}
