﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;

public class SwordControl : MonoBehaviour
{
    [SerializeField]

    Vector3 velocity;
    Vector3 prevPos;

    // Start is called before the first frame update
    void Start()
    {
        prevPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        velocity = (transform.position - prevPos) / Time.deltaTime;
        prevPos = transform.position;
    }

    public GameObject[] Slice(GameObject objectToSlice, Vector3 planeWorldPosition, Vector3 planeWorldDirection)
    {
        return objectToSlice.SliceInstantiate(planeWorldPosition, planeWorldDirection);
    }

    void OnCollisionStay(Collision collision)
    {
        Debug.Log("Collision");
        var obj = collision.gameObject;
        if (velocity.sqrMagnitude > 1f && obj.tag == "SphereEntity")
        {
            Debug.Log("Punctured");

            obj.tag = "SlicedSphere";

            var planeWorldPosition = collision.contacts[0].point;
            var planeWorldDirection = velocity;

            Debug.DrawRay(planeWorldPosition, planeWorldDirection);

            var parts = Slice(obj, planeWorldPosition, planeWorldDirection);
            obj.SetActive(false);

            foreach (var part in parts)
            {
                Destroy(part, 20);
                var rb = part.AddComponent<Rigidbody>();

                rb.interpolation = RigidbodyInterpolation.Interpolate;
                var col = part.AddComponent<MeshCollider>();
                col.convex = true;

                rb.AddExplosionForce(10, rb.position, 10);
            }
        }
    }
}
